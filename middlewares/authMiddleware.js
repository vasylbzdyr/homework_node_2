const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');

module.exports.authMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res
        .status(401)
        .json({message: `No Authorization http header found!`});
  }

  const [, jwtToken] = header.split(' ');

  if (!jwtToken) {
    return res.status(401).json({message: `No JWT token found!`});
  }

  try {
    req.user = jwt.verify(jwtToken, JWT_SECRET);
    next();
  } catch (err) {
    return res.status(400).json({message: 'Bad token'});
  }
};
