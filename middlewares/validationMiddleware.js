const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validatePassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),

    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateNoteText = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string()
        .alphanum()
        .min(1)
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};
