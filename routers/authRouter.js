const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../middlewares/helpers');
const {validateRegistration} = require('../middlewares/validationMiddleware');
const {login, register} = require('../controllers/authController');

router.post('/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(register));
router.post('/login', asyncWrapper(login));

module.exports = router;
