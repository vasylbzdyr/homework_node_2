const express = require('express');
const router = new express.Router();
const {authMiddleware} = require('../middlewares/authMiddleware');
const {existingProfileMiddleware} =
    require('../middlewares/existingProfileMiddleware');
const {asyncWrapper} = require('../middlewares/helpers');
const {validateNoteText} = require('../middlewares/validationMiddleware');


const {getAllNotes,
  addNote,
  getNoteById,
  updateNoteById,
  changeCompletedField,
  deleteNoteById} =
    require('../controllers/noteController');

router.get('/', authMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(getAllNotes));

router.post('/', authMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(validateNoteText),
    asyncWrapper(addNote));

router.get('/:id', authMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(getNoteById));

router.put('/:id', authMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(validateNoteText),
    asyncWrapper(updateNoteById));

router.patch('/:id', authMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(changeCompletedField));

router.put('/:id', authMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(validateNoteText),
    asyncWrapper(updateNoteById));

router.delete('/:id', authMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(deleteNoteById));

module.exports = router;
