const express = require('express');
const router = new express.Router();

const {
  existingProfileMiddleware,
} = require('../middlewares/existingProfileMiddleware');
const {authMiddleware} = require('../middlewares/authMiddleware');
const {validatePassword} = require('../middlewares/validationMiddleware');
const {
  asyncWrapper,
} = require('../middlewares/helpers');

const {
  getInformation,
  deleteAccount,
  changePassword,
} = require('../controllers/userController');

router.get('/', authMiddleware,
    asyncWrapper(existingProfileMiddleware),
    asyncWrapper(getInformation));

router.delete('/', authMiddleware, existingProfileMiddleware, deleteAccount);

router.patch('/',
    authMiddleware,
    asyncWrapper(validatePassword),
    changePassword);

module.exports = router;
