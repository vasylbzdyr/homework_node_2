const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

module.exports.getInformation = async (req, res, next) => {
  const user = await User.findById(req.user._id);

  return res.status(200).json({
    _id: user._id,
    username: user.username,
    createdDate: user.createdDate,
  });
};

module.exports.deleteAccount = async (req, res) => {
  await User.findByIdAndDelete(req.user._id);

  return res.status(200).json({
    'message': 'Success',
  });
};

module.exports.changePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user._id);
  const match = await bcrypt.compare(oldPassword, user.password);

  if (!match) {
    return res.status(400).json({message: `Wrong old password!`});
  }

  if (oldPassword === newPassword) {
    return res
        .status(400)
        .json({message: `New password is the same like oldpassword. 
        Please write other password.`});
  } else {
    user.password = await bcrypt.hash(newPassword, 10);

    await user.save();

    return res.status(200).json({message: 'Success'});
  }
};
