const {Note} = require('../models/noteModel');

module.exports.getAllNotes = async (req, res) => {
  try {
    let {offset = 0, limit = 3} = req.query;
    const {_id, username} = req.user;

    limit = limit > 10 ? 10 : limit;

    const notes = await Note.find({userId: _id}, {_v: 0})
        .skip(+offset)
        .limit(+limit);

    if (notes.length === 0) {
      return res.status(400).json({
        message: `Aren't notes for ${username}`,
      });
    }

    res.status(200).json({notes});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.addNote = async (req, res) => {
  const {text} = req.body;
  const {_id} = req.user;

  const note = new Note({
    userId: _id,
    completed: false,
    text: text,
  });

  await note.save();
  res.status(200).json({message: 'Success!'});
};

module.exports.getNoteById = async (req, res) => {
  const {id} = req.params;
  try {
    const note = await Note.findOne({_id: id}, {__v: 0});
    res.status(200).json({
      note: {
        '_id': note._id,
        'userId': note.userId,
        'completed': note.completed,
        'text': note.text,
        'createdDate': note.createdDate,
      },
    });
  } catch (err) {
    res.status(400).json({message: `No note with id '${id}' found!`});
  }
};

module.exports.updateNoteById = async (req, res) => {
  const {id} = req.params;
  const {text} = req.body;

  try {
    const note = await Note.findOne({_id: id}, {__v: 0});

    note.text = text;
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

module.exports.changeCompletedField = async (req, res) => {
  const {id} = req.params;

  try {
    const note = await Note.findOne({_id: id});

    note.completed = !note.completed;
    await note.save();

    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

module.exports.deleteNoteById = async (req, res) => {
  const {id} = req.params;

  try {
    const note = await Note.findOne({_id: id});

    if (!note) {
      return res.status(400).json({message: `No note with id '${id}' found!`});
    }

    await note.remove();

    return res.status(200).json({
      'message': 'Success',
    });
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};


// module.exports.getAllNotes = async (req, res) => {
//   const {offset, limit} = req.body;
//   const notes = await Note.find({userId: req.user._id}, {__v: 0})
//       .skip(offset)
//       .limit(limit);

//   res.status(200).json({notes: notes});
// };

// module.exports.createNote = async (req, res) => {
//   const text = req.body.text;
//   const userId = req.user._id;
//   const note = new Note({
//     userId: userId,
//     completed: false,
//     text: text,
//   });
//   if (!userId) {
//     res.status(400).json({message: 'You should log in.'});
//   }
//   await note.save();
//   res.status(200).json({message: 'Success!'});
// };

// module.exports.getNotebyId = async (req, res) => {
//   const {id} = req.params;
//   const note = await Note.findById(id, {_v: 0}).exec();
//   res.status(200).json({
//     note: {
//       _id: note._id,
//       userId: note.userId,
//       completed: note.completed,
//       text: note.text,
//       createdDate: note.createdDate,
//     },
//   });
// };

// module.exports.putNotebyId = async (req, res) => {
//   const {id} = req.params;
//   const {text} = req.body;
//   const note = await Note.findById(id, {_v: 0}).exec();
//   note.text = text;
//   await note.save();
//   res.status(200).json({message: 'Success'});
// };

// module.exports.patchNoteById = async (req, res) => {
//   const {id} = req.params;
//   const note = await Note.findById(id, {_v: 0}).exec();
//   note.completed = note.completed === true ? false : true;
//   await note.save();
//   res.status(200).json({message: 'Success'});
// };

// module.exports.deleteNotebyId = async (req, res) => {
//   const {id} = req.params;
//   await Note.findByIdAndRemove(id, req.body, function(err, data) {
//     if (!err) {
//       res.status(200).json({message: 'Success'});
//     }
//   });
// };
